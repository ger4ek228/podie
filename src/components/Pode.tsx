import { HapticFeedback, postEvent } from "@tma.js/sdk-react";
import Image from "next/image";
import React, { useEffect, useRef, useState } from "react";
import { Page } from "./Page/Page";
import coin from "/public/image.png";

const Pode = () => {
  const [counter, setCounter] = useState(0);
  const intervalRef = useRef<NodeJS.Timeout | null>(null);
  const [showCircle, setShowCircle] = useState(false);
  const [circlePosition, setCirclePosition] = useState({ x: 0, y: 0 });
  const holdTimeoutRef = useRef<NodeJS.Timeout | null>(null);

  const haptic = new HapticFeedback("6.3", postEvent);

  const startCounter = () => {
    haptic.impactOccurred("medium");
    if (intervalRef.current) return;
    setCounter(250);
    intervalRef.current = setInterval(() => {
      setCounter((prevCounter) => prevCounter - 1);
    }, 10);
  };

  const stopCounter = () => {
    if (intervalRef.current) {
      clearInterval(intervalRef.current);
      intervalRef.current = null;
    }
    setCounter(0);
  };

  const handleStart = (e: React.MouseEvent | React.TouchEvent) => {
    e.preventDefault();
    haptic.impactOccurred("light");
    const x = "touches" in e ? e.touches[0].clientX : e.clientX;
    const y = "touches" in e ? e.touches[0].clientY : e.clientY;
    setCirclePosition({ x, y });

    holdTimeoutRef.current = setTimeout(() => {
      setShowCircle(true);
      startCounter();
    }, 800); // 1 second hold
  };

  const handleStop = () => {
    if (holdTimeoutRef.current) {
      clearTimeout(holdTimeoutRef.current);
      holdTimeoutRef.current = null;
    }
    if (showCircle) {
      setShowCircle(false);
      setCounter(0);
      stopCounter();
    }
  };

  useEffect(() => {
    return () => stopCounter();
  }, []);

  useEffect(() => {
    if (counter === 0 && showCircle) {
      haptic.notificationOccurred("error");
      haptic.impactOccurred("heavy");
      setShowCircle(false);
      setCounter(0);
      stopCounter();
    }
  }, [counter]);

  const circleStyle: React.CSSProperties = {
    top: `${circlePosition.y - (counter + 100) / 2}px`,
    left: `${circlePosition.x - (counter + 10) / 2}px`,
    display: showCircle ? "block" : "none",
    height: `${counter}px`,
    width: `${counter}px`,
  };

  return (
    <Page title="Oleh The Bestovich">
      <div className="relative h-[450px] w-full max-w-[380px]">
        <div className="relative w-[380px] h-[450px]">
          <Image
            src={coin}
            fill
            alt="PODIE coin"
            onMouseUp={handleStop}
            onMouseLeave={handleStop}
            onTouchStart={handleStart}
            onTouchEnd={handleStop}
          />
          <div
            className="absolute border-2 border-yellow-500 rounded-full"
            style={circleStyle}
          />
        </div>
      </div>
    </Page>
  );
};

export default Pode;
